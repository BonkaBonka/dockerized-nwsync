################################################################################
# Image 0: Compile nwsync utilities
################################################################################
FROM docker.io/debian:testing

RUN apt-get update \
 && apt-get -y dist-upgrade
RUN apt-get -y install --no-install-recommends build-essential nim git ca-certificates

COPY nwsync /work/nwsync/

WORKDIR /work/nwsync/

RUN nimble build -d:release -y

################################################################################
# Image 1: Use the compiled utilities
################################################################################
FROM docker.io/debian:testing

RUN apt-get update \
 && apt-get -y dist-upgrade
RUN apt-get -y install --no-install-recommends ca-certificates rclone

COPY --from=0 /work/nwsync/bin/* /usr/local/bin/
